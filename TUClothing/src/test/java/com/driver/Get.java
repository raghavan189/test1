package com.driver;

import org.openqa.selenium.By;

import com.runner.Baseclass;

public class Get extends Baseclass {

	
	public String gettext (By element)
	{
		return driver.findElement(element).getText();
	}
}

package com.driver;

import org.openqa.selenium.By;

import com.runner.Baseclass;

public class Action extends Baseclass {

	
	public void ClickOnElement(By element)
	{
		driver.findElement(element).click();
	
	}
	
	
	public void UpdateElement(By element, String value)
	{
		driver.findElement(element).clear();
		driver.findElement(element).sendKeys(value);
	}
}


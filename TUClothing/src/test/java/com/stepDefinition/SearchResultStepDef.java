package com.stepDefinition;



import com.runner.Baseclass;

import cucumber.api.java.en.Then;

public class SearchResultStepDef extends Baseclass {

	@Then("^I Should See Valid Search Results$")
	public void i_Should_See_Valid_Search_Results() throws Throwable {
		searchresultspage.verifysearchresults();
	}
	@Then("^I Should See no results page$")
	public void then_I_Should_See_no_results_page() throws Throwable {
		searchresultspage.verifynosearchresult();
	}
	/*@Then("^I Should See  warning message$")
	public void i_Should_See_warning_message() throws Throwable {
	   
	}*/
	@Then("^I should see related category products$")
	public void i_should_see_related_category_products() throws Throwable {
	    searchresultspage.verifyrelatedcategory();
	}
	@Then("^I should see  product notfound page$")
	public void i_should_see_product_notfound_page() throws Throwable {
		searchresultspage.verifynoproductfound();
	}
	@Then("^I should see related product page$")
	public void i_should_see_related_product_page() throws Throwable {
	    searchresultspage.verifyproductnumber();
	}
	@Then("^I should see invalid product number page$")
	public void i_should_see_invalid_product_number_page() throws Throwable {
	    searchresultspage.verifyInvaidProdNumb();
	}
	@Then("^I should see warning message$")
	public void i_should_see_warning_message() throws Throwable {
	    searchresultspage.verifySpecialChar();
	}

	
}

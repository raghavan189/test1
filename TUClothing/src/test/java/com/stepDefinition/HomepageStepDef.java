package com.stepDefinition;

import com.runner.Baseclass;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomepageStepDef extends Baseclass {
	@Given("^I am in Homepage$")
	public void i_am_in_Homepage() throws Throwable {
		homepage.verifyHomepage();
	}

	
	/*@When("^I search with blank$")
	public void i_search_with_blank() throws Throwable {
	    
	}
	@When("^I enter valid product category$")
	public void i_enter_valid_product_category() throws Throwable {
	    
		homepage.SearchwithValidCategory();
	}
	@When("^I enter Invalid product category$")
	public void i_enter_Invalid_product_category() throws Throwable {
	    homepage.SearchwithInvalidCategory();
	}
	@When("^I enter valid product number$")
	public void i_enter_valid_product_number() throws Throwable {
		homepage.SearchwithValidProdNum();
	    
	}
	@When("^I enter invalid product number$")
	public void i_enter_invalid_product_number() throws Throwable {
	    homepage.SearchwithInValidProdNum();
	    
	}*/
	
	@When("^I enter symbols in textbox$")
	public void i_enter_symbols_in_textbox() throws Throwable {
	    homepage.SearchwithSpecialChar();
	}
	@When("^I search with \"([^\"]*)\" keyword$")
	public void i_search_with_keyword(String searchkeyword) throws Throwable {
		homepage.SearchwithKeyword(searchkeyword);
	    
	}
	@When("^I click TU store locator link$")
	public void i_click_TU_store_locator_link() throws Throwable {
	    homepage.clickonStorelocator();
	}
	@And("^enter the post code$")
	public void enter_the_post_code() throws Throwable {
		homepage.validPostCode();
	}

}

package com.stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.Baseclass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends Baseclass {
	
	private static String BASE_URL = "https://tuclothing.sainsburys.co.uk/" ;
	private static String Driver_Path = "C:\\Automation/chromedriver.exe";
	private static By COOKIE = By.cssSelector("#consent_prompt_submit");
	
	
	
	@Before 
		public void start() throws InterruptedException
		{
		System.setProperty("webdriver.chrome.driver", Driver_Path);
		driver = new ChromeDriver();
		driver.get(BASE_URL);
		Thread.sleep(2000);
		driver.findElement(COOKIE).click();
		
	}

	
	@After
	public void close()
	{
		driver.close();
	}
}

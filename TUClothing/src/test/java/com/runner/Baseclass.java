package com.runner;

import org.openqa.selenium.WebDriver;

import com.driver.Action;
import com.pages.HomePage;
import com.pages.SearchResultsPage;

public class Baseclass {

	public static WebDriver driver;
	public static HomePage homepage = new HomePage ();
	public static SearchResultsPage searchresultspage = new SearchResultsPage();
	public static Action action = new Action();
	
	
}

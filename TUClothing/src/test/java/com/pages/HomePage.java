package com.pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import com.runner.Baseclass;

public class HomePage  extends Baseclass {
	
	
	private static By SEARCH_TEXTBOX= By.cssSelector("#search");
	private static By SEARCH_BUTTON =By.cssSelector(".searchButton");
	
	
	public void verifyHomepage () throws InterruptedException
	{
		
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
		driver.manage().window().maximize(); }
		
	
	
	/*public void SearchWithblank()
	{
		action.UpdateElement(SEARCH_TEXTBOX, " ");
	}
	 public void SearchwithValidCategory()
	 {
		 action.UpdateElement(SEARCH_TEXTBOX, "Men");
		 action.ClickOnElement(SEARCH_BUTTON);
	 }
		
	 public void SearchwithInvalidCategory()
	 {
		 action.UpdateElement(SEARCH_TEXTBOX, "asdggmen");
		 action.ClickOnElement(SEARCH_BUTTON);
	 }
	 
	 public void SearchwithValidProdNum()
	 {
		 action.UpdateElement(SEARCH_TEXTBOX, "137338774");
		 action.ClickOnElement(SEARCH_BUTTON);
	 }
	 
	 public void SearchwithInValidProdNum()
	 {
		 action.UpdateElement(SEARCH_TEXTBOX, "333333333");
		 action.ClickOnElement(SEARCH_BUTTON);
	 } */
	 public void SearchwithSpecialChar()
	 {
		 action.UpdateElement(SEARCH_TEXTBOX, "#####");
		 action.ClickOnElement(SEARCH_BUTTON);
	 }
	 public void SearchwithKeyword(String searchkeyword )
	 {
		 
		 action.UpdateElement(SEARCH_TEXTBOX, searchkeyword);
		 action.ClickOnElement(SEARCH_BUTTON);
	 }
	 public void clickonStorelocator() 
	 {
		 driver.findElement(By.cssSelector("a[ href='/store-finder']")).click();
	 }
	 
	 
	 
	 public void validPostCode() throws InterruptedException
	 
		 {
				
				driver.findElement(By.cssSelector("[placeholder='Postcode or town']")).sendKeys("TW3 3AP");
				driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button--primary")).click();
				Thread.sleep(2000);
				}
	  
	 public void invalidpostcode()
	 {
		 driver.findElement(By.cssSelector("[placeholder='Postcode or town']")).sendKeys("ASEWQ123");
			driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button--primary")).click();
			Thread.sleep(2000);
	 }
	}


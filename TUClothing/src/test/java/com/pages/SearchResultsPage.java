package com.pages;

import org.junit.Assert;


import com.runner.Baseclass;

public class SearchResultsPage extends Baseclass {

	public void verifysearchresults() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=jeans", driver.getCurrentUrl());

	}

	public void verifynosearchresult() {

		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=xxyyxyyxyx", driver.getCurrentUrl());

	}

	/*
	 * public void verifyblank() { String warning =
	 * driver.findElement(By.cssSelector("#search-empty-errors")).getText();
	 * Assert.assertEquals("Please complete a product search", warning);
	 * 
	 * }
	 */
	public void verifyrelatedcategory()
	{
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/men/men?INITD=GNav-Men-Header",
				driver.getCurrentUrl());

	}
	public void verifynoproductfound()
	{
		
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=asdggmen", driver.getCurrentUrl());
	}
	public void verifyproductnumber()
	
	{
		Assert.assertEquals("Search results for: 137338774 | Tu clothing", driver.getTitle());
	}
	public void verifyInvaidProdNumb()
	{
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=333333333", driver.getCurrentUrl());
	}
	public void verifySpecialChar()
	{
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=%23%23%23%23%23", driver.getCurrentUrl());
	}
}

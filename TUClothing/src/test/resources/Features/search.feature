Feature: Search 
@test1
Scenario Outline: Verify Search with Valid Data

Given I am in Homepage 
When I search with "<keyword>" keyword
Then I Should See Valid Search Results

Examples:
|keyword|
|jeans|
|Men|
|137338774|

@test1
Scenario Outline: Verify Search with invalid data

Given I am in Homepage
When I search with "<keyword>" keyword
Then I Should See no results page

Examples:
|keyword|
|xxyyxyyxyx|
|asdggmen|
|333333333|


Scenario: Verify Search with blank 
Given I am in Homepage
When I search with blank
Then I Should See  warning message




Scenario: Verify functionality of search with Invalid product category
Given I am in Homepage
When I enter Invalid product category
Then I should see  product notfound page




Scenario: Verify functionality of search with invalid product number
Given I am in Homepage
When I enter invalid product number
Then I should see invalid product number page

Scenario: Verify functionality of search  with SpecialChar
Given I am in Homepage
When I enter symbols in textbox
Then I should see warning message




Scenario: Verify functionality of search with Invalid product cat
Given I am in Homepage
Then I should see  product notfound page





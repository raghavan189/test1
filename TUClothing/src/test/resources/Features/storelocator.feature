@test2
Feature: Store Locator
Scenario: Verify functionality of store locator
Given I am in Home page
When I click TU store locator link 
And enter the post code 
Then it should display the nearest stores 


Scenario: Verify functionality of store locator with invalid postcode
Given I am in Home page
When I click TU store locator link
And enter invalid postcode
Then it should display warning message
